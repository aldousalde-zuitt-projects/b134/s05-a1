1. select customerName from customers where country = "Philippines";

2. select contactLastName, contactFirstName from customers where customerName = "La Rochelle Gifts";

3. select productName, MSRP from products where productName = "The Titanic";

4. select firstName, lastName from employees where email = "jfirrelli@classicmodelcars.com";

5. select customerName from customers where state IS NULL;

6. select firstName, lastName, email from employees where lastName = "Patterson" and firstName = "Steve";

7. select customerName, country, creditLimit from customers where country != "USA" and creditLimit > 3000;

8. select customerName from customers where customerName like "%a%";

9. select customerNumber from orders where comments like "%DHL%";

10. select productLine from productlines where textDescription  like "%state of the art%";

11. select distinct country from customers;

12. select distinct status from orders;

13. select customerName, country from customers where country in ("USA", "France", "Canada");

14. select employees.firstName, employees.lastName, offices.city from employees join offices on employees.officeCode = offices.officeCode where offices.city = "Tokyo";

15. select customerName from customers join employees on customers.salesRepEmployeeNumber = employees.employeeNumber where employees.firstName = "Leslie" and employees.lastName = "Thompson";

16. select  customers.customerName, products.productName from customers join products  where customers.customerName = "Baane Mini Imports";

17. select employees.firstName, employees.lastName, customers.customerName, offices.country from offices left join employees on offices.officeCode = employees.officeCode left join customers on customers.country = offices.country where customers.country = offices.country; 

18. select lastName, firstName from employees where reportsTo = 1143;

19. select productName, MAX(MSRP) from products ;

20. select count(*) from customers where country = "UK";

21. select productLine, count(*) as count from products group by productLine;

22. select salesRepEmployeeNumber, count(*) as count from customers  group by salesRepEmployeeNumber;

23. select productName, quantityInStock from products where productLine = "planes" and quantityInStock < 1000;